/*

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package pingdom

import (
	"context"
	"fmt"
	observabilityv1beta1 "gitlab.com/tobbbles/pingdom-operator/pkg/apis/observability/v1beta1"
	"k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/tools/record"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	logf "sigs.k8s.io/controller-runtime/pkg/runtime/log"
	"sigs.k8s.io/controller-runtime/pkg/source"

	"github.com/russellcardullo/go-pingdom/pingdom"
)

var log = logf.Log.WithName("controller")

const (
	finalizer = "finalizer.pingdom.observability.tobbbl.es"
)

/**
* USER ACTION REQUIRED: This is a scaffold file intended for the user to modify with their own Controller
* business logic.  Delete these comments after modifying this file.*
 */

// Add creates a new Pingdom Controller and adds it to the Manager with default RBAC. The Manager will set fields on the Controller
// and Start it when the Manager is Started.
func Add(mgr manager.Manager) error {
	return add(mgr, newReconciler(mgr))
}

// newReconciler returns a new reconcile.Reconciler
func newReconciler(mgr manager.Manager) reconcile.Reconciler {
	return &ReconcilePingdom{Client: mgr.GetClient(), scheme: mgr.GetScheme(), recorder: mgr.GetRecorder("pingdom-controller")}
}

// add adds a new Controller to mgr with r as the reconcile.Reconciler
func add(mgr manager.Manager, r reconcile.Reconciler) error {
	// Create a new controller
	c, err := controller.New("pingdom-controller", mgr, controller.Options{Reconciler: r})
	if err != nil {
		return err
	}

	// Watch for changes to Pingdom
	err = c.Watch(&source.Kind{Type: &observabilityv1beta1.Pingdom{}}, &handler.EnqueueRequestForObject{})
	if err != nil {
		return err
	}

	return nil
}

var _ reconcile.Reconciler = &ReconcilePingdom{}

// ReconcilePingdom reconciles a Pingdom object
type ReconcilePingdom struct {
	client.Client
	scheme   *runtime.Scheme
	recorder record.EventRecorder
}

func (r *ReconcilePingdom) SetupFinalize(request reconcile.Request) error {
	instance := &observabilityv1beta1.Pingdom{}
	err := r.Get(context.TODO(), request.NamespacedName, instance)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}

	// Attach our finalizer if it is not currently present on a created instance
	if instance.ObjectMeta.DeletionTimestamp.IsZero() &&
		!finalizerPresent(instance.ObjectMeta.Finalizers, finalizer) {

		// Attach the finalizer to the instance and ensure the instance is correctly updated
		instance.ObjectMeta.Finalizers = append(instance.ObjectMeta.Finalizers, finalizer)
		if err := r.Update(context.TODO(), instance); err != nil {
			return err
		}
	}

	return nil
}

func (r *ReconcilePingdom) HandleFinalize(request reconcile.Request, client *pingdom.Client) error {
	instance := &observabilityv1beta1.Pingdom{}
	err := r.Get(context.TODO(), request.NamespacedName, instance)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}

	// Handle scheduled deletion of the instance with our finalizer on
	if !instance.ObjectMeta.DeletionTimestamp.IsZero() &&
		finalizerPresent(instance.ObjectMeta.Finalizers, finalizer) {

		// Clean up finalizer and status from the instance
		instance.ObjectMeta.Finalizers = removeFinalizer(instance.ObjectMeta.Finalizers, finalizer)
		if err := r.Update(context.TODO(), instance); err != nil {
			return err
		}

		// NOTE: This may be called multiple times so we must make the deletion idempotent. Before-hand we'll try to
		// get the check to see if it isn't present, in which case we can bail out early assuming it's deleted.
		res, _ := client.Checks.Read(instance.Status.ID)
		if res == nil {
			// If the check doesn't exist we've already cleaned up and can return happily
			return nil
		}

		// Delete our check from Pingdom
		_, err := client.Checks.Delete(instance.Status.ID)
		if err != nil {
			return err
		}

		return nil
	}

	return nil
}

// CreatePingdomClient handles Pingdom authentication.
//	We expect to authenticate with the Pingdom API through a secret within the cluster. Take the secret and use it
//	to authenticate with the pingdom client.
func (r *ReconcilePingdom) CreatePingdomClient(request reconcile.Request) (*pingdom.Client, error) {
	instance := &observabilityv1beta1.Pingdom{}
	err := r.Get(context.TODO(), request.NamespacedName, instance)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil, nil
		}
		return nil, err
	}

	secret := &v1.Secret{}
	if err := r.Get(context.TODO(), types.NamespacedName{
		Namespace: request.Namespace,
		Name:      instance.Spec.Secret,
	}, secret); err != nil {
		return nil, err
	}

	if _, ok := secret.Data["username"]; !ok {
		return nil, fmt.Errorf("pingdom: cannot authenticate - no username found in secret %s", instance.Spec.Secret)
	}

	if _, ok := secret.Data["password"]; !ok {
		return nil, fmt.Errorf("pingdom: cannot authenticate - no password found in secret %s", instance.Spec.Secret)
	}

	if _, ok := secret.Data["app_key"]; !ok {
		return nil, fmt.Errorf("pingdom: cannot authenticate - no app_key found in secret %s", instance.Spec.Secret)
	}

	return pingdom.NewClient(
		string(secret.Data["username"]),
		string(secret.Data["password"]),
		string(secret.Data["app_key"]),
	), nil
}

// Reconcile reads that state of the cluster for a Pingdom object and makes changes based on the state read
// and what is in the Pingdom.Spec
// +kubebuilder:rbac:groups=observability.tobbbl.es,resources=pingdoms,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=observability.tobbbl.es,resources=pingdoms/status,verbs=get;update;patch
// +kubebuilder:rbac:groups="",resources=events,verbs=create;patch
func (r *ReconcilePingdom) Reconcile(request reconcile.Request) (reconcile.Result, error) {
	// Pingdom Client
	// 	Construct the client to interact with the Pingdom API up here as it's required in cleanup logic of finalizers.
	client, err := r.CreatePingdomClient(request)
	if err != nil {
		return reconcile.Result{}, err
	}

	// Finalizers
	//	Handle all finalization logic for the instance including attaching the finalizer to instance metadata and
	//	ensuring 3rd party pingdom checks are removed along side the instance itself.
	if err := r.SetupFinalize(request); err != nil {
		return reconcile.Result{}, err
	}

	if err := r.HandleFinalize(request, client); err != nil {
		return reconcile.Result{}, err
	}

	// Fetch the Pingdom instance
	instance := &observabilityv1beta1.Pingdom{}
	if err := r.Get(context.Background(), request.NamespacedName, instance); err != nil {
		if errors.IsNotFound(err) {
			// Object not found, return.  Created objects are automatically garbage collected.
			// For additional cleanup logic use finalizers.
			return reconcile.Result{}, nil
		}
		// Error reading the object - requeue the request.
		return reconcile.Result{}, err
	}

	// Pingdom Check Equality
	// 	We want to see if the instance has already been created in Pingdom, and if it has whether it's equal or not
	// 	this was we can update the check if required
	if instance.Status.ID != 0 {
		// Try to get the remote check from Pingdom
		res, err := client.Checks.Read(instance.Status.ID)
		if err != nil {
			return reconcile.Result{}, err
		}

		// If our check matches the instance spec then there's nothing to do, we're kosher
		if !matchesSpec(res, instance.Spec) {
			// Re-create our check from our spec, and update it remotely in Pingdom
			check, err := createPingdomCheck(instance.Spec)
			if err != nil {
				return reconcile.Result{}, err
			}

			// After updating we're done
			if _, err := client.Checks.Update(instance.Status.ID, check); err != nil {
				return reconcile.Result{}, err
			}

			// TODO(tobbbles): This is really naive. Evaluate if it's the right solution, or should we invoke 1 more API
			// call to Pingdom to get the status.
			// Update our instance state to match the newly updated spec
			instance.Status.Name = check.PostParams()["name"]

			if err := r.Status().Update(context.TODO(), instance); err != nil {
				return reconcile.Result{}, err
			}

			return reconcile.Result{}, nil
		}
	} else {
		check, err := createPingdomCheck(instance.Spec)
		if err != nil {
			return reconcile.Result{}, err
		}

		res, err := client.Checks.Create(check)
		if err != nil {
			return reconcile.Result{}, err
		}

		r.recorder.Event(
			instance,
			"Normal",
			"Created",
			fmt.Sprintf("Created pingdom check %d", res.ID),
		)

		log.Info("creating check", "status", instance.Status)

		instance.Status.ID = res.ID
		instance.Status.Name = res.Name
		if err := r.Status().Update(context.Background(), instance); err != nil {
			return reconcile.Result{}, err
		}
	}

	return reconcile.Result{}, nil
}

// finalizerPresent checks for the presence of a finalizer in a collection of them
func finalizerPresent(collection []string, finalizer string) bool {
	for _, val := range collection {
		if val == finalizer {
			return true
		}
	}
	return false
}

// removeFinalizer rebuilds a string collection without the given finalizer string.
// NOTE(tobbbles): This is real nasty in terms of puking over the garbage collector, but there's no point
// prematurely optimising.
func removeFinalizer(collection []string, finalizer string) []string {
	var result []string

	for _, val := range collection {
		if val != finalizer {
			result = append(result, val)
		}
	}

	return result
}

func createPingdomCheck(spec observabilityv1beta1.PingdomSpec) (pingdom.Check, error) {
	var check pingdom.Check

	switch spec.Type {
	case "http":
		check = &pingdom.HttpCheck{Name: spec.Name, Hostname: spec.Host, Resolution: spec.Resolution}
	case "tcp":
		check = &pingdom.TCPCheck{Name: spec.Name, Hostname: spec.Host, Resolution: spec.Resolution}
	case "ping":
		check = &pingdom.PingCheck{Name: spec.Name, Hostname: spec.Host, Resolution: spec.Resolution}
	default:
		return nil, fmt.Errorf("pingdom: invalid check type - %s is invalid", spec.Type)
	}

	return check, nil
}

// This function is responsible for deciding if a remote *pingdom.CheckResponse matches the CRD spec
func matchesSpec(response *pingdom.CheckResponse, spec observabilityv1beta1.PingdomSpec) bool {
	if response.Name != spec.Name {
		return false
	}

	if response.Resolution != spec.Resolution {
		return false
	}

	if response.Type.Name != spec.Type {
		return false
	}

	if response.Hostname != spec.Host {
		return false
	}

	return true
}
