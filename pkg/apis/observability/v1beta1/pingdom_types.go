/*

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1beta1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// Important: Run "make" to regenerate code after modifying this file

// PingdomSpec defines the desired state of Pingdom
type PingdomSpec struct {
	// +kubebuilder:validation:Maximum=100
	// +kubebuilder:validation:Minimum=1
	Secret string `json:"secret"`

	// +kubebuilder:validation:Maximum=100
	// +kubebuilder:validation:Minimum=1
	Name string `json:"name"`

	// +kubebuilder:validation:Enum=http,httpcustom,tcp,ping,dns,udp,smtp,pop3,imap
	Type string `json:"type"`

	// +kubebuilder:validation:Maximum=100
	// +kubebuilder:validation:Minimum=1
	Host string `json:"host"`

	// +kubebuilder:validation:Enum=1,5,15,30,60
	Resolution int `json:"resolution"`
}

// PingdomStatus defines the observed state of Pingdom
type PingdomStatus struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// Pingdom is the Schema for the pingdoms API
// +k8s:openapi-gen=true
// +kubebuilder:subresource:status
type Pingdom struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   PingdomSpec   `json:"spec,omitempty"`
	Status PingdomStatus `json:"status,omitempty"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// PingdomList contains a list of Pingdom
type PingdomList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []Pingdom `json:"items"`
}

func init() {
	SchemeBuilder.Register(&Pingdom{}, &PingdomList{})
}
