# pingdom-operator

A Kubernetes Operator for creating Pingdom checks from your cluster, built with Kubebuilder.

Supported Check types:
`HTTP`
`TCP`
`Ping`

## Notes

##### Secret
Your Pingdom check must have a secret in the same namespace (configured via the pingdom spec.secret).

This secret ought to have the following opaque key values correlating to your Pingdom Account.
* username
* password
* app_key

## Development

## Requirements
- [Kubebuilder](https://github.com/kubernetes-sigs/kubebuilder)
- [Kustomize](https://github.com/kubernetes-sigs/kustomize)
- Docker
- Go
- Kubectl

## Building
#### Building the Operator
If you have all requirements installed, you can build the operator with
`make`.

#### Pushing Image
The operator image is currently hosted under `tobbbles/pingdom-operator` on Dockerhub. If authenticated with the repository, you could `make docker-build` and `make docker-push` respectively.

#### Deploy our Operator
The following command will piece all of operator kustomize manifests together and bring up the operator in the cluster, along with the correct RBAC configurations. 

`make deploy`

#### Deploying a pingdom check

We provide a sample manifest for a pingdom check in `config/samples`. 
`kubectl get pingdom --all-namespaces`


